def bin_to_str(b):
    lol = lambda lst, sz: [lst[i:i+sz] for i in range(0, len(lst), sz)]
    b_split = lol(b,7)
    w = ''
    for e in b_split:
        if len(e) < 7:
            e += [0]*(7-len(e))
        bin = ''.join(str(el) for el in e)
        n = int(bin, 2)
        w += chr(n)
    return w