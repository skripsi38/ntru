import json, base64

def str_to_bin(s):
    bin = [format(ord(i), 'b') for i in s]
    bin_output = []
    for b in bin:
        elem = [int(e) for e in b]
        if len(elem) < 7:
            elem = ([0]*(7-len(elem))) + elem
        bin_output += elem
    return bin_output

def bin_to_str(b):
    lol = lambda lst, sz: [lst[i:i+sz] for i in range(0, len(lst), sz)]
    b_split = lol(b,7)
    w = ''
    for e in b_split:
        if len(e) < 7:
            e += [0]*(7-len(e))
        bin = ''.join(str(el) for el in e)
        n = int(bin, 2)
        w += chr(n)
    return w

def encode64(bin_arr):
    json_encoded_list = ' '.join([str(i) for i in bin_arr])
    b64_encoded_list = base64.b64encode(json_encoded_list.encode('utf-8'))
    return b64_encoded_list

def decode64(s):
    b = base64.b64decode(s).decode('utf-8')
    b = b.split()
    b = [int(i) for i in b]
    return b

if __name__ == "__main__":
    s = "hello world"
    bin_list = str_to_bin(s)
    bin_list_b64_encoded = encode64(bin_list)
    print(bin_list)
    print(bin_list_b64_encoded)
    bin_list_b64_decoded = decode64(bin_list_b64_encoded)
    print(bin_list_b64_decoded)