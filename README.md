# NTRU

## Perhatian
Project ini adalah hasil fork dari https://github.com/Sapphirine/ntru. Disusun untuk tugas akhir atau skripsi Program Studi Informatika, Fakultas Teknologi dan Sains Data, Universitas Sebelas Maret.

## Petunjuk
Program yang dapat dijalankan adalah program dengan nama file **example_bobalice.py** yang mendemonstrasikan enkripsi-dekripsi menggunakan algoritma NTRU. Di dalam program sudah disediakan function untuk mencoba beberaca level keamanan pada algoritma NTRU.

Untuk mengganti level keamanan dapat dilakukan dengan mengganti parameter pemanggilan fungsi get_parameter seperti di bawah ini
```py
N, p, q = get_parameter(112)
```
Parameter adalah level keamanan yang direkomendasikan NIST yaitu 80, 112, 128, 192, 256.