# Description of this example is provided in NTRU.md

from ntru import *
from strtobin import str_to_bin, encode64, decode64
from bintostr import bin_to_str
import time


# Parameter
def get_parameter(security_level):
    p = 3
    q = 2048
    if security_level == 80:
        N = 251
    elif security_level == 112:
        N = 401
    elif security_level == 128:
        N = 443
    elif security_level == 160:
        N = 491
    elif security_level == 192:
        N = 593
    elif security_level == 256:
        N = 743
    else:
        raise ValueError('security level is invalid')
    return (N, p, q)

# Parameter
N, p, q = get_parameter(112)

f=[1,0,1,-1,0,-1,1]
g=[-1,0,1,1,0,0,-1]
d=2

#Bob
print("Bob  akan membuat kunci")
print(f"N={N},p={p} and q={q}")
Bob=Ntru(N,p,q)
print(f"f(x)= {f}")
print(f"g(x)= {g}")
print(f"d   = {d}")
t1 = time.perf_counter_ns()
Bob.genPublicKey(f,g,d)
t2 = time.perf_counter_ns()
bob_pub_key=Bob.getPublicKey()
pub_key_encoded = encode64(bob_pub_key)
print(" ------- Bob Public Key -------")
print(pub_key_encoded)
print(" ------- End Bob Public Key -------")
print(f"Key Generation Time: {(t2-t1)/pow(10,9)} s\n")

print("-------------------------------------------------")
#Alice
print("Alice akan mengenkripsi pesan")
Alice=Ntru(N,p,q)
Alice.setPublicKey(bob_pub_key)
m = "hello world"
msg = str_to_bin(m)
print(f"Pesan Alice : {m}")
print(f"Pesan Alice (biner): {msg}")
rand_poly=[-1,-1,1,1]
print(f"Polinomial sembarang alice : {rand_poly}")
t1_encrypt = time.perf_counter_ns()
encrypt_msg=Alice.encrypt(msg,rand_poly)
t2_encrypt = time.perf_counter_ns()
encrypt_msg_encoded = encode64(encrypt_msg)
print(" ------- Encrypted Message -------")
print(encrypt_msg_encoded)
print(" ------- End Encrypted Message -------")
print(f"Encryption Time: {(t2_encrypt-t1_encrypt)/pow(10,9)} s\n")

print("-------------------------------------------------")
#BOB
print("Bob akan mendekripsi pesan")
t1_decrypt = time.perf_counter_ns()
d = Bob.decrypt(encrypt_msg)
t2_decrypt = time.perf_counter_ns()
print("Pesan terdekripsi (biner) : ", d)
w_d = bin_to_str(d)
print("Pesan terdekripsi : ", w_d)
print(f"Decryption Time: {(t2_decrypt-t1_decrypt)/pow(10,9)} s\n")