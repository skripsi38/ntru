from ntru import Ntru
import base64

Bob=Ntru(1499,3,2048)
f=[1,1,-1,0,-1,1]
g=[-1,0,1,1,0,0,-1]
d=2
print(f"f(x)= {f}")
print(f"g(x)= {g}")
print(f"d   = {d}")
Bob.genPublicKey(f,g,2)
h = ' '.join([str(i) for i in Bob.h])

pub_key = f'{Bob.N}#{Bob.p}#{Bob.q}#[{h}]'
pub_key_encoded = base64.b64encode(pub_key.encode('utf-8'))
pub_key_decoded = base64.b64decode(pub_key_encoded)
pub_key_decoded_lst = pub_key_decoded.decode('utf-8').split("#")
print(pub_key)
print(pub_key_encoded)
print(pub_key_decoded)
print(pub_key_decoded_lst)
h = pub_key_decoded_lst[3].replace('[', '')
h = h.replace(']', '')
h = h.split(' ')
h = [int(i) for i in h]
N = int(pub_key_decoded_lst[0])
p = int(pub_key_decoded_lst[1])
q = int(pub_key_decoded_lst[2])
print(N)
print(p)
print(q)
print(h)


f = ' '.join([str(i) for i in Bob.f])
f_p = ' '.join([str(i) for i in Bob.f_p])
g = ' '.join([str(i) for i in Bob.g])
priv_key = f'[{f}]#[{f_p}]#[{g}]'
priv_key_encoded = base64.b64encode(priv_key.encode('utf-8'))
priv_key_decoded = base64.b64decode(priv_key_encoded)
priv_key_decoded_lst = priv_key_decoded.decode('utf-8').split("#")
priv_key_decoded_lst_int = []
for i in priv_key_decoded_lst:
    i = i.replace('[', '')
    i = i.replace(']', '')
    temp = i.split(' ')
    temp = [int(i) for i in temp]
    priv_key_decoded_lst_int.append(temp)
print(priv_key)
print(priv_key_encoded)
print(priv_key_decoded)
print(priv_key_decoded_lst)
print(priv_key_decoded_lst_int)